package com.vinsguru.saga.controller;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.vinsguru.dto.OrchestratorRequestDTO;
import com.vinsguru.dto.OrchestratorResponseDTO;
import com.vinsguru.saga.service.IorchestratorService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/orchestrator")
public class OrchestratorController {
	@Autowired
	private IorchestratorService OrchestratorService;

	@PostMapping("/createOrder")
	public Mono<ResponseEntity<OrchestratorResponseDTO>>responseController(@RequestBody OrchestratorRequestDTO requestDTO) {
		
		return OrchestratorService.orderProduct(requestDTO).map(book -> new ResponseEntity<OrchestratorResponseDTO>(book, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	
	
//    @Bean
//    public Function<Flux<OrchestratorRequestDTO>, Flux<OrchestratorResponseDTO>> processor(){
//        return flux -> flux
//                            .flatMap(dto -> this.OrchestratorService.orderProduct(dto))
//                            .doOnNext(dto -> System.out.println("Status : " + dto.getStatus()));
//    }
}
