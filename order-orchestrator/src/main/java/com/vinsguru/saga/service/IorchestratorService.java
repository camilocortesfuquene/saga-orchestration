package com.vinsguru.saga.service;

import com.vinsguru.dto.InventoryRequestDTO;
import com.vinsguru.dto.OrchestratorRequestDTO;
import com.vinsguru.dto.OrchestratorResponseDTO;
import com.vinsguru.dto.PaymentRequestDTO;
import com.vinsguru.enums.OrderStatus;

import reactor.core.publisher.Mono;

public interface IorchestratorService {

	public Mono<OrchestratorResponseDTO> orderProduct(final OrchestratorRequestDTO requestDTO);

	public Mono<OrchestratorResponseDTO> revertOrder(final Workflow workflow, final OrchestratorRequestDTO requestDTO);

	public Workflow getOrderWorkflow(OrchestratorRequestDTO requestDTO);

	public OrchestratorResponseDTO getResponseDTO(OrchestratorRequestDTO requestDTO, OrderStatus status);

	public PaymentRequestDTO getPaymentRequestDTO(OrchestratorRequestDTO requestDTO);

	public InventoryRequestDTO getInventoryRequestDTO(OrchestratorRequestDTO requestDTO);
}
